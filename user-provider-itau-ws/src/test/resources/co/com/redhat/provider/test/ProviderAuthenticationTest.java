package co.com.redhat.provider.test;

import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.keycloak.component.ComponentModel;
import org.keycloak.credential.CredentialInput;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;

import co.com.redhat.sso.provider.user.ItauBaseDatosUserStorageProvider;

public class ProviderAuthenticationTest {
	private KeycloakSession mockedKeycloakSession;
	private ComponentModel mockComponentModel;
	private RealmModel mockRealModel;
	private UserModel mockUserModel;
	private CredentialInput mockCredentialInput;

	@BeforeEach
	void setUp() {
		this.mockedKeycloakSession = mock(KeycloakSession.class);
		this.mockComponentModel = mock(ComponentModel.class);
		this.mockRealModel = mock(RealmModel.class);
		this.mockUserModel = mock(UserModel.class);
		this.mockCredentialInput = mock(CredentialInput.class);
	}
	
	@Test
	public void testIsValid() {
		Boolean isValid = false;
		mockUserModel.setUsername("U8300383191_8300383191");	
		
		ItauBaseDatosUserStorageProvider itauBDSP = new ItauBaseDatosUserStorageProvider(mockedKeycloakSession, mockComponentModel);
		isValid = itauBDSP.isValid(mockRealModel, mockUserModel, mockCredentialInput);
		System.out.println(isValid);
	}

}
