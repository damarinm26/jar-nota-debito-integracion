package co.com.redhat.sso.util;

public class Constant {
	
	public static final String ORIGINATOR_NAME = "Portal";
	public static final Integer ORIGINATOR_TYPE = 4;
	public static final String STATUS_CODE = "000";
	public static final String SERVER_STATUS_CODE = "00";
	public static final String URL_WEB_SERVICE = "http://10.186.11.91:24200/services/security/ValidatePassword?wsdl";

}
